import 'package:flutter/material.dart';

class CustomTheme {
  static ThemeData get lightTheme {
    return ThemeData(
      //   primaryColor: Colors.grey,
      // backgroundColor: Colors.black,
      //   scaffoldBackgroundColor: Colors.grey.shade200,
      fontFamily: 'Roboto', //3
      textTheme: textTheme,
      primarySwatch: Colors.blue,
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          primary: primaryColor,
        ),
      ),
      inputDecorationTheme: InputDecorationTheme(
        border: InputBorder.none,
        labelStyle: TextStyle(color: Colors.pink),
        // hintStyle: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
      ),
    );
  }

  static ButtonStyle primaryButtonStyle = ElevatedButton.styleFrom(
      primary: CustomTheme.primaryColor.withOpacity(0.6),
      onPrimary: Colors.white70,
      side: BorderSide(
          color: CustomTheme.primaryColor.withOpacity(0.12), width: 1.0));

  static ButtonStyle dangerButtonStyle = ElevatedButton.styleFrom(
      primary: Colors.red.withOpacity(0.6),
      onPrimary: Colors.red,
      side: BorderSide(color: Colors.red.withOpacity(0.12), width: 1.0));

  static ButtonStyle successButtonStyle = ElevatedButton.styleFrom(
      primary: Color(0xFF8E7DBE).withOpacity(0.5),
      onPrimary: Colors.white70,
      side: BorderSide(color: Colors.green.withOpacity(0.12), width: 1.0));

  static const Color primaryColor = Color(0xFF444545);
  static const Color secondaryColor = Color(0xFF2A2D3E);
  static const Color primaryTextColor = Color(0xFF444545);
//   const bgColor = Color(0xFF212332);

//   const defaultPadding = 16.0;

  static const Color nearlyWhite = Color(0xFFFAFAFA);
  static const Color white = Color(0xFFFFFFFF);
//   static const Color background = Color(0xFF212332);
  static const Color background = Color(0xFFE7DFC6);
  static const Color nearlyDarkBlue = Color(0xFF2633C5);

  static const Color nearlyBlue = Color(0xFF00B6F0);
  static const Color nearlyBlack = Color(0xFF213333);
  static const Color grey = Color(0xFF3A5160);
  static const Color dark_grey = Color(0xFF313A44);

  static const Color darkText = Color(0xFF253840);
  static const Color darkerText = Color(0xFF17262A);
  static const Color lightText = Color(0xFF4A6572);
  static const Color deactivatedText = Color(0xFF767676);
  static const Color dismissibleBackground = Color(0xFF364A54);
  static const Color spacer = Color(0xFFF2F2F2);
  static const String fontName = 'Roboto';

  static const TextTheme textTheme = TextTheme(
    headline4: display1,
    headline5: headline,
    headline6: title,
    subtitle2: subtitle,
    bodyText2: body2,
    bodyText1: body1,
    caption: caption,
  );

  static const TextStyle display1 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 36,
    letterSpacing: 0.4,
    height: 0.9,
    color: darkerText,
  );

  static const TextStyle headline = TextStyle(
      fontFamily: fontName,
      fontWeight: FontWeight.bold,
      fontSize: 18,
      letterSpacing: 0.27,
      color: primaryTextColor);

  static const TextStyle title = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 16,
    letterSpacing: 0.18,
    color: Colors.white70,
  );

  static const TextStyle subtitle = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: darkText,
  );

  static const TextStyle body2 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: 0.2,
    color: darkText,
  );

  static const TextStyle body1 = TextStyle(
      fontFamily: fontName,
      fontWeight: FontWeight.w400,
      fontSize: 12,
      letterSpacing: -0.05,
      color: primaryTextColor);

  static const TextStyle caption = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 12,
    letterSpacing: 0.2,
    color: lightText, // was lightText
  );
}
