// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// ignore_for_file: public_member_api_docs

import 'package:app_video/provider/exercise_list_provider.dart';
import 'package:app_video/screens/exercises_list_screen.dart';
import 'package:app_video/screens/test_jump_screen.dart';
import 'package:app_video/services/service_locator.dart';
import 'package:app_video/theme/custom_theme.dart';
import 'package:app_video/viewmodel/create_exercise_viewmodel.dart';
import 'package:app_video/viewmodel/test_jump_viewmodel.dart';
import 'dart:io';

/// An example of using the plugin, controlling lifecycle and playback of the
/// video.

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:app_video/screens/create_exercise_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupServiceLocator();
  await SystemChrome.setPreferredOrientations(<DeviceOrientation>[
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown
  ]).then((_) => runApp(App()));
}

// BuildContext MAIN_CONTEXT;

// void showSnackbar(SnackBar snackBar) {
//   ScaffoldMessenger.of(MAIN_CONTEXT).showSnackBar(snackBar);
// }

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AppState();
}

class _AppState extends State<App> {
  int _currentScreenIndex = 0;
  PageController _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness:
          Platform.isAndroid ? Brightness.light : Brightness.light,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.light,
    ));
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
              create: (context) => CreateExerciseViewModel()),
          ChangeNotifierProvider(
            create: (context) => ExerciseListProvider(),
          ),
          ChangeNotifierProvider(create: (context) => TestJumpViewModel())
        ],
        child: MaterialApp(
          theme: CustomTheme.lightTheme,
          title: "Your Jump",
          home: CreateExerciseScreen(),
        ));
  }

  void onTapBottomBtn(int index) {
    print(index);
    setState(() {
      _currentScreenIndex = index;
    });
    _pageController.animateToPage(index,
        duration: Duration(milliseconds: 500), curve: Curves.easeIn);
  }
}

class HexColor extends Color {
  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));

  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }
}
