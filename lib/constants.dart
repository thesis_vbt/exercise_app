import 'package:flutter/material.dart';

// New exercise keys
GlobalKey<FormState> NEW_EXERCISE_KEY_NAME = GlobalKey<FormState>();
GlobalKey<FormState> NEW_EXERCISE_VIDEO = GlobalKey<FormState>();
GlobalKey<FormState> EDIT_EXERCISE_KEY_NAME = GlobalKey<FormState>();

const String API_BASE_URL = "192.168.1.78:8000";
