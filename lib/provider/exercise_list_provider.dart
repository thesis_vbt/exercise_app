import 'dart:convert';
import 'dart:io';

import 'package:app_video/models/exercise.dart';
import 'package:app_video/models/repetition.dart';
import 'package:app_video/models/video.dart';
import 'package:app_video/services/service_locator.dart';
import 'package:app_video/services/webapi/web_api.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';

class ExerciseListProvider extends ChangeNotifier {
  List<Exercise> exercises = [];

  void refresh() {
    exercises = [];
    var api = GetIt.I.get<WebApi>();
    api.resetPage();
    fetchExercises();
  }

  void fetchExercises() async {
    print("fetchExercises() called...");
    var api = GetIt.I.get<WebApi>();
    var test = await api.fetchNextPage();
    print("Test: $test");
    if (test == null || test.isEmpty) return;
    exercises.addAll(test);
    notifyListeners();
  }
}
