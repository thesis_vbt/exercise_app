import 'dart:io';

import 'package:app_video/constants.dart';
import 'package:app_video/main.dart';
import 'package:app_video/models/exercise.dart';
import 'package:app_video/theme/custom_theme.dart';
import 'package:app_video/viewmodel/create_exercise_viewmodel.dart';
import 'package:app_video/widgets/video_player_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:carousel_slider/carousel_slider.dart';

class VideoList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _VideoListState();
}

class _VideoListState extends State<VideoList> with TickerProviderStateMixin {
  GlobalKey _videoListKey = GlobalKey();

  CarouselController _carouselController = CarouselController();
  int _currentCarouselPage = 1;
  final List<Widget> _videoPlayersList = [];
  int _videosIndexCounter = 1;

  @override
  void initState() {
    super.initState();
    addNewVideo();
    _pageViewController.addListener(() {
      setState(() {
        _currentCarouselPage = _pageViewController.page!.round() + 1;
      });
    });
    // I'm not calling addNewVideo to avoid calling setState since it causes a weird flicker
    // _videoPlayersList.add(_createVideoWidget(0));
  }

  int _videosCount = 0;
  List<UniqueKey> _videos = [];
  PageController _pageViewController = PageController();

  Widget _createVideoWidget(Key key) {
    var widgetIndex = _videos.length - 1;
    NEW_EXERCISE_VIDEO = GlobalKey<FormState>();
    var videoPlayer = VideoPlayer(
      index: widgetIndex,
      formKey: NEW_EXERCISE_VIDEO,
      aspectRatio: 9 / 16,
      hidePerspectiveDescriptionForm: false,
      onPerspectiveChanged: (text) {
        {
          var exercise = context.read<CreateExerciseViewModel>();
          exercise.setVideoFileDescription(widgetIndex, text);
        }
      },
      onSourceChange: (filePath) {
        var exercise = context.read<CreateExerciseViewModel>();
        exercise.setVideoFile(File(filePath));
        exercise.setCurrentVideoController(widgetIndex);
      },
      onControllerChange: (controller) {
        print("onControllerChange called!!!");
        context
            .read<CreateExerciseViewModel>()
            .setVideoController(widgetIndex, controller);
      },
    );
    return Dismissible(
      key: key,
      direction: DismissDirection.none,
      confirmDismiss: (direction) {
        if (_videos.length == 1) return Future.value(false);
        return Future.value(true);
      },
      onDismissed: (direction) {
        _videos.remove(key);

        // setState(() {});
      },
      child: Container(
        color: Colors.transparent,
        // padding: EdgeInsets.all(15),
        //   margin: EdgeInsets.all(5.0),
        child: Stack(
          children: <Widget>[
            videoPlayer,
          ],
        ),
      ),
    );
  }

  void addNewVideo() {
    setState(() {
      //   _videoPlayersList.add(_createVideoWidget(_videoPlayersList.length));
      _videos.add(UniqueKey());
      //   _videosCount++;
    });
    // print("Videos List: $_videos");
  }

  void removeVideo(int index) {
    // var viewModel = context.read<NewExerciseViewModel>();
    // viewModel.removeVideo(index);
    // viewModel.removeVideoController(index);
    // setState(() {
    //   _videoPlayersList.removeAt(index);
    // });
    // _pageViewController.
  }

  Widget build(BuildContext context) {
    // RenderBox box = context.findRenderObject();
    // Offset position = box.localToGlobal(Offset.zero);
    var exercise = context.read<CreateExerciseViewModel>();
    return Container(
      key: _videoListKey,
      //   margin: EdgeInsets.only(top: 10),
      //   padding: EdgeInsets.all(5),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Flexible(
            child: Container(
              //   padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  // color: Colors.red,
                  ),
              child: AspectRatio(
                aspectRatio: 9 / 16,
                child: _createVideoWidget(_videos[0]),
                // carouselController: _carouselController,
                // options: CarouselOptions(
                //   onPageChanged: (int index, CarouselPageChangedReason reason) {
                // setState(() {
                //   _currentCarouselPage = index;
                // });

                // context
                // .read<NewExerciseViewModel>()
                // .setCurrentVideoController(index);
                //   },
                //   aspectRatio: 16 / 9,
                //   enableInfiniteScroll: false,
                //   enlargeCenterPage: true,
                //   viewportFraction: 1,

                //   scrollDirection: Axis.vertical,
                // enlargeStrategy: CenterPageEnlargeStrategy.height,
                // ),
              ),
            ),
          ),
          //   Container(
          //     child: Row(
          //       mainAxisAlignment: MainAxisAlignment.end,
          //       children: [
          //         Container(
          //           child: Container(
          //             padding: EdgeInsets.all(5),
          //             child: Text("$_currentCarouselPage/${_videos.length}",
          //                 style: TextStyle(color: Colors.white70)),
          //           ),
          //         ),
          // Container(
          //     margin: EdgeInsets.only(right: 5),
          //     child: ElevatedButton.icon(
          //         style: CustomTheme.primaryButtonStyle,
          //         icon: Icon(Icons.add_outlined),
          //         label: Text("Add video"),
          //         onPressed: () {
          //           addNewVideo();
          //         })),
          // Container(
          //   margin: EdgeInsets.only(right: 10),
          //   child: ElevatedButton(
          //       style: CustomTheme.dangerButtonStyle,
          //       child: Icon(Icons.remove_circle_outline_sharp),
          //       onPressed: () {
          //       }),
          // )
        ],
      ),
    );
  }
}
