import 'package:app_video/models/exercise.dart';
import 'package:app_video/models/repetition.dart';
import 'package:app_video/viewmodel/create_exercise_viewmodel.dart';
import 'package:better_player/better_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:app_video/theme/custom_theme.dart';

class RepetitionsListWidget extends StatefulWidget {
  final BetterPlayerController videoController;
  RepetitionsListWidget({required this.videoController});

  @override
  State<StatefulWidget> createState() => _RepetitionListState();
}

class ListItem {
  Repetition repetition;
  bool isExpanded = false;

  ListItem(this.repetition);
}

class _RepetitionListState extends State<RepetitionsListWidget> {
  int selectedRepIx = -1;

  Duration? startRepTime;
  Duration? endRepTime;
//   Map<int, bool> _isExpandedList = {};
  List<ListItem> _items = [];

  @override
  Widget build(BuildContext context) {
    var exercise = context.watch<CreateExerciseViewModel>();

    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            //   margin: EdgeInsets.only(top: 5, left: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 15, left: 20),
                  child: Text(
                    "Repetitions",
                    style: CustomTheme.title,
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      color: CustomTheme.nearlyDarkBlue,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(15),
                          bottomRight: Radius.circular(15.0))),
                  child: Row(
                    children: [],
                  ),
                )
              ],
            ),
          ),
          Expanded(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15.0),
              child: ListView(
                padding: EdgeInsets.only(top: 10),
                shrinkWrap: true,
                // expansionCallback: (index, isExpanded) {
                //   setState(() {
                //     exercise.repetitions[index].isExpanded = !isExpanded;
                //   });
                // },
                children: exercise.repetitions
                    .asMap()
                    .map(
                      (int index, Repetition repetition) {
                        return MapEntry(index,
                            makeRepetitionTile(context, repetition, index + 1));
                        // isExpanded: _isExpandedList[index]);
                      },
                    )
                    .values
                    .toList(),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Visibility(
                  visible: exercise.isAllVideosWithDescriptionAndVideo,
                  child: ElevatedButton.icon(
                    //   style: CustomTheme.elevatedButtonStyle,
                    icon: Icon(Icons.arrow_drop_down_circle_rounded),
                    label: Text("Mark beginning"),
                    style: CustomTheme.primaryButtonStyle,
                    onPressed: exercise.isAllVideosWithDescriptionAndVideo
                        ? () {
                            print("onPresset:");
                            print(exercise.currentController);
                            setState(() {
                              startRepTime = exercise.currentController
                                  .videoPlayerController?.value.position;
                            });
                          }
                        : null,
                  ),
                ),
              ),
              Expanded(
                child: Visibility(
                  visible: exercise.isAllVideosWithDescriptionAndVideo,
                  child: ElevatedButton.icon(
                    style: CustomTheme.primaryButtonStyle,
                    icon: Icon(Icons.arrow_drop_up_rounded),
                    label: Text("Mark end"),
                    onPressed: (startRepTime != null)
                        ? () {
                            endRepTime = exercise.currentController
                                .videoPlayerController?.value.position;

                            exercise.addRepetition(startRepTime, endRepTime);

                            startRepTime = null;
                            endRepTime = null;
                            setState(() {});
                          }
                        : null,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  final TextStyle timeLabelStyle = TextStyle(
      color: Colors.black, fontWeight: FontWeight.normal, fontSize: 12);
  final TextStyle timeStampStyle =
      TextStyle(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 13);

  Container makeRepetitionTile(
          BuildContext context, Repetition rep, int index) =>
      Container(
        margin: EdgeInsets.only(left: 10, right: 10),
        child: Card(
          elevation: 15,
          color: Colors.white.withAlpha(200),
          child: ExpansionTile(
            leading: Container(
              padding: EdgeInsets.only(right: 12.0),
              decoration: new BoxDecoration(
                  border: new Border(
                      right: new BorderSide(width: 1.0, color: Colors.black))),
              child: Text(index.toString(),
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.w900)),
            ),
            children: [Text("VBT")],
            title: Column(
              children: [
                Row(children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      // color: Colors.red,
                      child: Text("Starts at ", style: timeLabelStyle),
                    ),
                  ),
                  Expanded(
                      flex: 2,
                      child: Container(
                          // color: Colors.green,
                          child: Text("${rep.startTime?.format()}",
                              style: timeStampStyle)))
                ]),
                Row(children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      // color: Colors.red,
                      child: Text("Ends at ", style: timeLabelStyle),
                    ),
                  ),
                  Expanded(
                      flex: 2,
                      child: Container(
                          // color: Colors.green,
                          child: Text("${rep.endTime?.format()}",
                              style: timeStampStyle)))
                ])
              ],
            ),
            //   subtitle: Row(
            //     children: <Widget>[
            //   Icon(Icons.linear_scale, color: Colors.yellowAccent),
            //   Text(" Intermediate", style: TextStyle(color: Colors.white))
            //     ],
            //   ),
            // );
            trailing: IconButton(
              icon: Icon(Icons.delete_outline),
              color: Colors.redAccent,
              onPressed: () {
                var exercise = context.read<CreateExerciseViewModel>();
                exercise.removeRepetition(rep);
              },
            ),
          ),
        ),
      );
}
