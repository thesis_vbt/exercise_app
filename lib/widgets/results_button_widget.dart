import 'package:app_video/viewmodel/test_jump_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

enum _State { WAITING_FOR_INPUT, READY_TO_ANALYZE, PROCESSING, FINISHED }

class ResultsButton extends StatefulWidget {
  ResultsButton({Key? key}) : super(key: key);

  @override
  _ResultsButtonState createState() => _ResultsButtonState();
}

class _ResultsButtonState extends State<ResultsButton> {
  _State _currentState = _State.WAITING_FOR_INPUT;

  void updateState(TestJumpViewModel vm) {
    // if (_currentState == _State.WAITING_FOR_INPUT &&
    // vm.landingTime != null &&
    // vm.takeOffTime != null) {
    //   _currentState = _State.READY_TO_ANALYZE;
    // } else if (_currentState == _State.READY_TO_ANALYZE && )
  }

  @override
  Widget build(BuildContext context) {
    // var vm = context.watch<TestJumpViewModel>();
    // updateState(vm);

    switch (_currentState) {
      case _State.WAITING_FOR_INPUT:
        return Container();
      case _State.READY_TO_ANALYZE:
        return ElevatedButton.icon(
            onPressed: () {},
            icon: Icon(Icons.analytics),
            label: Text("Analyze!"));
      case _State.PROCESSING:
        return ElevatedButton.icon(
            onPressed: () {},
            icon: Icon(Icons.access_alarm),
            label: Text("Processing..."));
      case _State.FINISHED:
        return Text("Finished!");
    }

    return Container();
  }
}
