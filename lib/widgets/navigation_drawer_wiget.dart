import 'package:app_video/screens/create_exercise_screen.dart';
import 'package:app_video/screens/exercises_list_screen.dart';
import 'package:app_video/screens/test_jump_screen.dart';
import 'package:app_video/theme/custom_theme.dart';
import 'package:flutter/material.dart';

enum Screens {
  CREATE_EXERCISE,
  SEARCH_EXERCISE,
  TEST_JUMP,
  TEST_DEADLIFT,
}

class NavigationDrawer extends StatefulWidget {
  NavigationDrawer({Key? key}) : super(key: key);

  @override
  _NavigationDrawerState createState() => _NavigationDrawerState();
}

class _NavigationDrawerState extends State<NavigationDrawer> {
  final padding = EdgeInsets.symmetric(horizontal: 20);
  Screens selectedScreen = Screens.CREATE_EXERCISE;

//   static final _NavigationDrawerState _instance =
//       _NavigationDrawerState._internal();

//   factory _NavigationDrawerState() {
//     return _instance;
//   }

//   _NavigationDrawerState._internal();

  Widget buildItem(
      {required String text,
      required IconData icon,
      required VoidCallback onClick,
      required Screens index}) {
    final color = Colors.white;
    final hoverColor = Colors.white70;

    return ListTile(
      selected: (selectedScreen == index),
      leading: Icon(
        icon,
        color: color,
      ),
      title: Text(
        text,
        style: TextStyle(color: color, fontSize: 14),
      ),
      hoverColor: hoverColor,
      onTap: onClick,
    );
  }

  void selectItem(BuildContext context, Screens index) {
    // if (selectedIndex == index) {
    //   Navigator.of(context).pop();
    //   return;
    // }
    selectedScreen = index;

    switch (index) {
      case Screens.CREATE_EXERCISE:
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => CreateExerciseScreen()));
        break;
      case Screens.TEST_JUMP:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => TestJumpScreen()));
        break;
      case Screens.SEARCH_EXERCISE:
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => ExercisesListScreen()));
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Material(
        color: CustomTheme.secondaryColor,
        child: ListView(
          padding: padding,
          children: <Widget>[
            const SizedBox(height: 40),
            buildItem(
              text: "Create exercise",
              icon: Icons.create,
              index: Screens.CREATE_EXERCISE,
              onClick: () => selectItem(context, Screens.CREATE_EXERCISE),
            ),
            const SizedBox(height: 16),
            buildItem(
              text: "Search exercises",
              icon: Icons.search,
              index: Screens.SEARCH_EXERCISE,
              onClick: () => selectItem(context, Screens.SEARCH_EXERCISE),
            ),
            Divider(
              color: CustomTheme.primaryColor,
            ),
            buildItem(
              text: "Jump test",
              icon: Icons.sports_handball,
              index: Screens.TEST_JUMP,
              onClick: () => selectItem(context, Screens.TEST_JUMP),
            ),
            buildItem(
              text: "Deadlift test",
              icon: Icons.sports_hockey_outlined,
              index: Screens.TEST_DEADLIFT,
              onClick: () => selectItem(context, Screens.TEST_DEADLIFT),
            ),
          ],
        ),
      ),
    );
  }
}
