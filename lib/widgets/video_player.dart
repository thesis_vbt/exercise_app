import 'dart:io';

import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerWrapper extends StatefulWidget {
  VideoPlayerWrapper(
      {required this.videoPath,
      required this.newKey,
      required this.videoController,
      required this.onControllerChange})
      : super(key: newKey);

  final void Function(ChewieController) onControllerChange;
  final VideoPlayerController videoController;
  final String videoPath;
  final Key newKey;

  @override
  _VideoPlayeWrapperrState createState() => _VideoPlayeWrapperrState();
}

class _VideoPlayeWrapperrState extends State<VideoPlayerWrapper> {
  late ChewieController _chewieController;
  late VideoPlayerController _videoController;

  @override
  void initState() {
    super.initState();
    // _videoController = VideoPlayerController.file(File(widget.videoPath));
    _chewieController = ChewieController(
        videoPlayerController: widget.videoController, autoInitialize: true);

    widget.onControllerChange(_chewieController);
  }

  @override
  Widget build(BuildContext context) {
    return Chewie(
      controller: _chewieController,
    );
  }

  @override
  void dispose() {
    _videoController.dispose();
    _chewieController.dispose();
    super.dispose();
  }
}
