import 'package:app_video/models/exercise.dart';
import 'package:app_video/viewmodel/create_exercise_viewmodel.dart';
import 'package:app_video/widgets/video_player.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:better_player/better_player.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_ffmpeg/flutter_ffmpeg.dart';
import 'package:flutter_ffmpeg/media_information.dart';
import 'package:flutter_ffmpeg/stream_information.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:provider/provider.dart';
import 'package:app_video/theme/custom_theme.dart';
import 'package:video_player/video_player.dart';

import '../constants.dart';

class VideoPlayer extends StatefulWidget {
  final int index;
  final double aspectRatio;
  final ValueChanged<String> onSourceChange;
  final void Function(ChewieController) onControllerChange;
  final void Function(String) onPerspectiveChanged;
  final bool hidePerspectiveDescriptionForm;
  final GlobalKey<FormState>? formKey;

  const VideoPlayer(
      {Key? key,
      required this.index,
      required this.aspectRatio,
      required this.onSourceChange,
      required this.onControllerChange,
      required this.onPerspectiveChanged,
      this.hidePerspectiveDescriptionForm = true,
      this.formKey})
      : super(key: key);

  @override
  _VideoPlayerState createState() => _VideoPlayerState();
}

class _VideoPlayerState extends State<VideoPlayer>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  VideoPlayerController? _videoController;
  Duration? startRepTime;
  Duration? endRepTime;

  final _videoFile = ImagePicker();
  final _videoFps = -1;
  int _videoFramePeriod = 13; // assume 30fps

  late AnimationController _videoSettingsBarAnimationController;
  late Animation<double> _videoSettingsBarAnimation;

  late CreateExerciseViewModel _viewModel;

  String? _videoSourcePath;
  Key? _videoKey;

  void changeVideo(String path) {
    setState(() {
      _videoController = VideoPlayerController.file(File(path));
      _videoSourcePath = path;
      _videoKey = UniqueKey();
    });
  }

  /// Use to control the slight opacity change when a video source is set
  bool _fullOpacity = true;
  @override
  void initState() {
    super.initState();

    startRepTime = null;
    endRepTime = null;
    _videoSettingsBarAnimationController = AnimationController(
        duration: const Duration(milliseconds: 350), vsync: this, value: 1.0);

    _videoSettingsBarAnimation = CurvedAnimation(
        parent: _videoSettingsBarAnimationController,
        curve: Curves.fastOutSlowIn);

    print("INIT STATE, index: ${widget.index}");
    // Add this video controller to the list
  }

  @override
  void dispose() {
    _videoSettingsBarAnimationController.dispose();
    print("DISPOSING VIDEO PLAYER WIDGET INDEX: ${widget.index}");
    // _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Flexible(
            child: IntrinsicHeight(
              child: Stack(
                children: [
                  AspectRatio(
                      aspectRatio: widget.aspectRatio,
                      child: _videoSourcePath != null
                          ? VideoPlayerWrapper(
                              videoController: _videoController!,
                              videoPath: _videoSourcePath!,
                              newKey: _videoKey!,
                              onControllerChange: (controller) {
                                widget.onControllerChange(controller);
                              },
                            )
                          : Container(
                              color: Colors.black,
                            )),
                  Container(
                    child: ScaleTransition(
                      scale: _videoSettingsBarAnimation,
                      child: AnimatedOpacity(
                        opacity: _fullOpacity ? 0.99 : 0.9,
                        duration: Duration(milliseconds: 1500),
                        child: Container(
                          color: CustomTheme.background,
                          child: Center(
                            child: IntrinsicWidth(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Visibility(
                                    visible:
                                        !widget.hidePerspectiveDescriptionForm,
                                    child: Form(
                                      //   key: widget.formKey,
                                      child: Container(
                                        child: TextFormField(
                                          style: CustomTheme.body1,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.only(
                                                top: -5, left: 10, bottom: -5),
                                            fillColor: CustomTheme.background,
                                            filled: true,
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                borderSide: BorderSide(
                                                    color: CustomTheme
                                                        .background
                                                        .withAlpha(150))),
                                            hintStyle: CustomTheme.body1,
                                            labelStyle: CustomTheme.body1,
                                            hintText: 'Perspective description',
                                          ),
                                          validator: (value) {
                                            if (value == "") {
                                              return "Perspective description cannot be empty";
                                            }
                                            return null;
                                          },
                                          onChanged:
                                              widget.onPerspectiveChanged,
                                        ),
                                      ),
                                    ),
                                  ),
                                  ElevatedButton.icon(
                                    style: CustomTheme.primaryButtonStyle,
                                    onPressed: () async {
                                      var file = await _chooseVideo();
                                      if (file != null) {
                                        widget
                                            .onSourceChange(file.absolute.path);
                                        setState(() {
                                          _fullOpacity = false;
                                        });
                                      }
                                    },
                                    icon: Icon(Icons.video_call),
                                    label: Text("Choose video source"),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 0,
                    left: 0,
                    child: Container(
                      child: IconButton(
                        color: !_isCloseVideoSettingsBar
                            ? Colors.red.shade400
                            : Colors.white,
                        icon: !_isCloseVideoSettingsBar
                            ? Icon(Icons.close)
                            : Icon(Icons.video_settings_rounded),
                        onPressed: () async {
                          setState(() {
                            _isCloseVideoSettingsBar =
                                !_isCloseVideoSettingsBar;

                            _fullOpacity = true;
                          });

                          if (_videoSettingsBarAnimationController
                              .isCompleted) {
                            await _videoSettingsBarAnimationController
                                .reverse();
                          } else {
                            await _videoSettingsBarAnimationController
                                .forward();
                          }
                        },
                      ),
                    ),
                  ),
                  Positioned(
                    right: 0,
                    bottom: 60,
                    child: ElevatedButton(
                        child: Text("Next frame"),
                        onPressed: () async {
                          print("Advancing?");
                          if (_videoController != null) {
                            print("YEP");
                            Duration p =
                                await _videoController!.position as Duration;
                            print(p);
                            _videoController!.seekTo(
                                p + Duration(milliseconds: _videoFramePeriod));
                          }
                        }),
                  ),
                  Positioned(
                    left: 0,
                    bottom: 60,
                    child: ElevatedButton(
                        child: Text("Prev frame"),
                        onPressed: () async {
                          if (_videoController != null) {
                            Duration p =
                                await _videoController!.position as Duration;
                            print(p);
                            _videoController!.seekTo(
                                p - Duration(milliseconds: _videoFramePeriod));
                          }
                        }),
                  )
                ],
              ),
            ),
          ),
        ],
        // ),
        // ],
        // ),
      ),
    );
  }

  bool _isCloseVideoSettingsBar = false;

  @override
  bool get wantKeepAlive => true;

  Future<File?> _chooseVideo() async {
    File? f = await _pickVideo();
    if (f != null) {
      changeVideo(f.path);
    }
    return f;
  }

  // This funcion will help you to pick a Video File
  _pickVideo() async {
    PickedFile? pickedFile =
        await _videoFile.getVideo(source: ImageSource.gallery);

    if (pickedFile == null) {
      return null;
    }

    final FlutterFFprobe flutterFFprobe = FlutterFFprobe();
    StreamInformation? videoStream;

    var info = await flutterFFprobe.getMediaInformation(pickedFile.path);

    for (var stream in info.getStreams() ?? List.empty()) {
      if (stream.getAllProperties()["codec_type"] == "video") {
        videoStream = stream;
      }
    }

    if (videoStream != null) {
      try {
        var avgFrameRate =
            videoStream.getAllProperties()["avg_frame_rate"].toString();

        int numerator = int.parse(avgFrameRate.split("/")[0]);
        int denominator = int.parse(avgFrameRate.split("/")[1]);
        int fps = (numerator / denominator).round();

        _videoFramePeriod = (1000 / fps).round();
      } catch (FormatException) {
        _videoFramePeriod = 13;
        print("Failed to retrieve FPS from video metadata. Assuming 30fps.");
      }
    } else {
      _videoFramePeriod = 13;
      print("Failed to retrieve FPS from video metadata. Assuming 30fps.");
    }

    print("Time period: $_videoFramePeriod");
    return File(pickedFile.path);
  }
}
