// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pose_output.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PoseOutput _$PoseOutputFromJson(Map<String, dynamic> json) {
  return PoseOutput(
    json['id'] as int,
    json['pose_output'] as String?,
  );
}

Map<String, dynamic> _$PoseOutputToJson(PoseOutput instance) =>
    <String, dynamic>{
      'id': instance.id,
      'pose_output': instance.poseOutput,
    };
