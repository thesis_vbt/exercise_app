import 'package:json_annotation/json_annotation.dart';

part 'pose_output.g.dart';

@JsonSerializable()
class PoseOutput {
  late int id;

  @JsonKey(name: "pose_output")
  late String? poseOutput;

  PoseOutput(this.id, this.poseOutput);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory PoseOutput.fromJson(Map<String, dynamic> json) =>
      _$PoseOutputFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$PoseOutputToJson(this);

//   @override
//   String toString() {
  // return "$perspectiveDescription @ $videoFile";
//   }
}
