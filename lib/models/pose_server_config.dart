import 'package:json_annotation/json_annotation.dart';

part 'pose_server_config.g.dart';

@JsonSerializable()
class PoseServerConfig {
  late int id;
  late String name;

  late String description;

  late String server;

  PoseServerConfig(this.id, this.name, this.description, this.server);

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory PoseServerConfig.fromJson(Map<String, dynamic> json) =>
      _$PoseServerConfigFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$PoseServerConfigToJson(this);
}
