import 'dart:io';
import 'package:app_video/models/pose_output.dart';
import 'package:json_annotation/json_annotation.dart';

import '../constants.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'video.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Video {
  int? id;
  @JsonKey(name: "perspective_description")
  String perspectiveDescription;
//   @JsonKey(fromJson: _pathToFile, toJson: _fileToPath)
  @JsonKey(name: "video_file")
  String videoFile;
  @JsonKey(name: "created_at")
  DateTime? createdAt;
  @JsonKey(name: "updated_at")
  DateTime? updatedAt;

  @JsonKey(name: "thumbnail")
  Uri? imageUrl;

  List<PoseOutput> poses = [];

//   static Uri _strToUri(String url) {
//     return url == null || url == "" ? null : Uri.parse(API_BASE_URL + url);
//   }

  static File _pathToFile(String path) {
    return File(path);
  }

  static String? _fileToPath(File file) {
    return file.absolute.path;
  }

  Video(this.perspectiveDescription, this.videoFile);

  factory Video.empty() {
    return Video("", "");
  }

//   @override
//   String toString() {
//     return "${this.perspectiveDescription} => ${this.videoFile != null ? this.videoFile.path : "null"}";
//   }

//   Map<String, dynamic> toJson() => {
//         "perspective_description": perspectiveDescription,
//         "video_file": videoFile.absolute.path
//       };

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Video.fromJson(Map<String, dynamic> json) => _$VideoFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$VideoToJson(this);

  @override
  String toString() {
    return "$perspectiveDescription @ $videoFile";
  }
}
