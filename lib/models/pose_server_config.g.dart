// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pose_server_config.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PoseServerConfig _$PoseServerConfigFromJson(Map<String, dynamic> json) {
  return PoseServerConfig(
    json['id'] as int,
    json['name'] as String,
    json['description'] as String,
    json['server'] as String,
  );
}

Map<String, dynamic> _$PoseServerConfigToJson(PoseServerConfig instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'server': instance.server,
    };
