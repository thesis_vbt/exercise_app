import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'repetition.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Repetition {
  @JsonKey(
      fromJson: _timestampToDuration,
      toJson: _durationToTimestamp,
      name: "start_timestamp")
  Duration? startTime;
  @JsonKey(
      fromJson: _timestampToDuration,
      toJson: _durationToTimestamp,
      name: "end_timestamp")
  Duration? endTime;
  @JsonKey(ignore: true)
  bool isExpanded = false;

  DateTime? createdAt;
  DateTime? updatedAt; // used in the expansionlistpanel

  Repetition(this.startTime, this.endTime);

  static Duration _timestampToDuration(String timestamp) {
    if (timestamp == null) return Duration();
    var tokens = timestamp.split(":");
    var microTokens = tokens.last.split(".");
    int hours, minutes, seconds, microsec = 0;
    hours = int.parse(tokens[0]);
    minutes = int.parse(tokens[1]);

    if (microTokens.length > 1) {
      seconds = int.parse(microTokens[0]);
      microsec = int.parse(microTokens[1]);
    } else {
      seconds = int.parse(tokens[2]);
    }

    return Duration(
        hours: hours,
        minutes: minutes,
        seconds: seconds,
        microseconds: microsec);
  }

  static String _durationToTimestamp(Duration? duration) {
    return duration.toString();
  }

  @override
  String toString() {
    return "${startTime?.format()} - ${endTime?.format()}";
  }

//   Map<String, dynamic> toJson() => {
//         'start_timestamp': startTime.format(),
//         'end_timestamp': endTime.format()
//       };
  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Repetition.fromJson(Map<String, dynamic> json) =>
      _$RepetitionFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$RepetitionToJson(this);
}

extension CustomFormat on Duration {
  String format() {
    return "0" + this.toString();
  }
}
