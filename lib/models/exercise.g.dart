// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'exercise.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Exercise _$ExerciseFromJson(Map<String, dynamic> json) {
  return Exercise(
    json['name'] as String,
  )
    ..id = json['id'] as int?
    ..createdAt = json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..videos = (json['videos'] as List<dynamic>)
        .map((e) => Video.fromJson(e as Map<String, dynamic>))
        .toList()
    ..repetitions = (json['repetitions'] as List<dynamic>)
        .map((e) => Repetition.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ExerciseToJson(Exercise instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'created_at': instance.createdAt?.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'videos': instance.videos,
      'repetitions': instance.repetitions,
    };
