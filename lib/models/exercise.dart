import 'dart:convert';
import 'dart:io';
import 'package:app_video/models/video.dart';
import 'package:app_video/models/repetition.dart';
import 'package:better_player/better_player.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'exercise.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Exercise extends ChangeNotifier {
  Exercise(String name) {
    this.name = name;
  }
  int? id;
  late String name;
  @JsonKey(name: "created_at")
  DateTime? createdAt;
  @JsonKey(name: "updated_at")
  DateTime? updatedAt;

//   Map<int, Video> videoFiles = {};
  List<Video> videos = [];
  List<Repetition> repetitions = [];

//   String get name => _name;
//   set name(String name) => _name = name;
//   List<Repetition> get repetitions => _repetitions;
//   Map<int, VideoFile> get videos => _videoFiles;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Exercise.fromJson(Map<String, dynamic> json) =>
      _$ExerciseFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$ExerciseToJson(this);

  @override
  String toString() {
    String reps = "";
    repetitions.forEach((element) {
      reps += element.toString();
    });
    return "Exercise: $name => with $reps repetitions and ${videos.length} videos";
  }

//   toJson() {
//     var dict = Map<String, dynamic>();
//     dict["name"] = name;
//     dict["repetitions"] = [];
//     dict["videos"] = [];
//     repetitions.forEach((element) {
//       dict["repetitions"].add(element.toJson());
//     });
//     videos.forEach((key, value) {
//       dict["videos"].add(value.toJson());
//     });

//     return dict;
//   }

//   factory fromJson(Map<String, dynamic> json) {}
}
