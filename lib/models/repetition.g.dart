// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'repetition.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Repetition _$RepetitionFromJson(Map<String, dynamic> json) {
  return Repetition(
    Repetition._timestampToDuration(json['start_timestamp'] as String),
    Repetition._timestampToDuration(json['end_timestamp'] as String),
  )
    ..createdAt = json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String)
    ..updatedAt = json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String);
}

Map<String, dynamic> _$RepetitionToJson(Repetition instance) =>
    <String, dynamic>{
      'start_timestamp': Repetition._durationToTimestamp(instance.startTime),
      'end_timestamp': Repetition._durationToTimestamp(instance.endTime),
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };
