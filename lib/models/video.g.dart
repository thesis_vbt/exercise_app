// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'video.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Video _$VideoFromJson(Map<String, dynamic> json) {
  return Video(
    json['perspective_description'] as String,
    json['video_file'] as String,
  )
    ..id = json['id'] as int?
    ..createdAt = json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String)
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..imageUrl = json['thumbnail'] == null
        ? null
        : Uri.parse(json['thumbnail'] as String)
    ..poses = (json['poses'] as List<dynamic>)
        .map((e) => PoseOutput.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$VideoToJson(Video instance) => <String, dynamic>{
      'id': instance.id,
      'perspective_description': instance.perspectiveDescription,
      'video_file': instance.videoFile,
      'created_at': instance.createdAt?.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'thumbnail': instance.imageUrl?.toString(),
      'poses': instance.poses,
    };
