import 'package:app_video/main.dart';
import 'package:app_video/models/exercise.dart';
import 'package:app_video/provider/exercise_list_provider.dart';
import 'package:app_video/screens/exercise_details_screen.dart';
import 'package:app_video/theme/custom_theme.dart';
import 'package:app_video/widgets/navigation_drawer_wiget.dart';
import 'package:app_video/widgets/search_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class ExercisesListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ExercisesListScreenState();
}

class _ExercisesListScreenState extends State<ExercisesListScreen> {
  String query = "";

  late ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.offset >=
              _scrollController.position.maxScrollExtent &&
          !_scrollController.position.outOfRange) {
        var list = context.read<ExerciseListProvider>();
        list.fetchExercises();
      }
      if (_scrollController.offset <=
              _scrollController.position.minScrollExtent &&
          !_scrollController.position.outOfRange) {
        //   setState(() {
        // message = "reach the top";
        //   },
        //   );
      }
    });

    var list = context.read<ExerciseListProvider>();
    list.refresh();
  }

  @override
  Widget build(BuildContext context) {
    var list = context.watch<ExerciseListProvider>();
    return Scaffold(
      drawer: NavigationDrawer(),
      body: Container(
        color: CustomTheme.background,
        child: Column(
          children: [
            Builder(
              builder: (context) => Container(
                margin: EdgeInsets.only(top: 40),
                child: Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 10),
                      child: IconButton(
                          color: CustomTheme.primaryColor,
                          icon: Icon(Icons.list),
                          onPressed: () {
                            Scaffold.of(context).openDrawer();
                          }),
                    ),
                    Expanded(
                      child: SearchWidget(
                          text: query,
                          onChanged: onChanged,
                          hintText: "Search by exercise name"),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: GridView.builder(
                  controller: _scrollController,
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 300,
                      childAspectRatio: 16 / 9,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 20),
                  itemCount: list.exercises.length,
                  itemBuilder: (BuildContext ctx, index) {
                    return _buildExerciseCard(list.exercises[index]);
                  }),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildExerciseCard(Exercise exercise) {
    return Container(
      decoration: BoxDecoration(
          color: CustomTheme.secondaryColor,
          borderRadius: BorderRadius.circular(10)),
      child: Stack(
        children: [
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          ExerciseDetailsScreen(exercise: exercise)));
              print("Exercise id: ${exercise.id}");
              print("Reps: ${exercise.repetitions}");
            },
            child: exercise.videos.length > 0
                ? ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Center(
                        child: Image.network(
                            exercise.videos[0].imageUrl.toString())))
                : Container(
                    decoration: BoxDecoration(
                        color: CustomTheme.secondaryColor,
                        borderRadius: BorderRadius.circular(10)),
                    child: Center(
                        child: Text(
                      "No image found",
                      style: TextStyle(color: Colors.white),
                    )),
                  ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: Container(
              color: Colors.black,
              padding: EdgeInsets.only(top: 5),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Text(
                    exercise.name,
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  void onChanged(String value) {
    print(value);
  }
}
