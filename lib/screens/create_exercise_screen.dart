import 'dart:convert';
import 'dart:io';

import 'package:app_video/models/exercise.dart';
import 'package:app_video/models/repetition.dart';
import 'package:app_video/theme/custom_theme.dart';
import 'package:app_video/viewmodel/create_exercise_viewmodel.dart';
import 'package:app_video/widgets/SABT.dart';
import 'package:app_video/widgets/navigation_drawer_wiget.dart';
import 'package:app_video/widgets/video_list.dart';
import 'package:flutter/material.dart';
import 'package:app_video/widgets/video_player_widget.dart';
import 'package:app_video/widgets/repetitions_list_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:app_video/main.dart';
import 'package:http/http.dart' as http;
import "package:app_video/constants.dart";

class CreateExerciseScreen extends StatefulWidget {
  @override
  _CreateExerciseScreenState createState() => _CreateExerciseScreenState();
}

class _CreateExerciseScreenState extends State<CreateExerciseScreen> {
  @override
  void initState() {
    super.initState();
  }

  Widget _createExerciseNameTextField(BuildContext context) {
    // NEW_EXERCISE_KEY_NAME = GlobalKey<FormState>();

    return Form(
      //   key: NEW_EXERCISE_KEY_NAME,
      child: TextFormField(
          style: CustomTheme.headline,
          decoration: InputDecoration(
              hintStyle: CustomTheme.headline, hintText: "Exercise name"),
          validator: (value) {
            if (value == "") {
              return "Exercise name cannot be empty";
            }
            return null;
          },
          onChanged: (text) {
            var exercise = context.read<CreateExerciseViewModel>();
            exercise.name = text;
          }),
    );
  }

  Widget buildSaveButton(BuildContext context) {
    var vm = context.read<CreateExerciseViewModel>();
    return Container(
      margin: EdgeInsets.only(right: 10),
      child: ElevatedButton.icon(
          label: Text("Save"),
          style: CustomTheme.successButtonStyle,
          icon: Icon(Icons.save),
          onPressed: vm.repetitions.length > 0
              ? () async {
                  try {
                    await vm.save();
                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text("Exercise saved!")));
                  } catch (e) {
                    print(e);

                    ScaffoldMessenger.of(context)
                        .showSnackBar(SnackBar(content: Text(e.toString())));
                  }
                }
              : null),
    );
  }

  int _selectedDrawerIndex = -1;
  void selectIndex(int index) {
    setState(() {
      _selectedDrawerIndex = index;
    });
  }

  Widget build(BuildContext context) {
    var exercise = context.watch<CreateExerciseViewModel>();

    return Scaffold(
      backgroundColor: Colors.transparent,
      drawer: NavigationDrawer(),
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            elevation: 4,
            shadowColor: Colors.black,
            title: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Flexible(child: _createExerciseNameTextField(context)),
                buildSaveButton(context)
              ],
            ),
            leading: Builder(
              builder: (context) => IconButton(
                icon: Icon(Icons.menu),
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
                color: CustomTheme.primaryColor,
              ),
            ),
            // expandedHeight: MediaQuery.of(context).size.height * 0.06,
            // expandedHeight: MediaQuery.of(context).size.height * 0.8,
            backgroundColor: CustomTheme.background,
            foregroundColor: Colors.red,
            floating: true,
            pinned: false,
            snap: true,
          ),
          //   SliverToBoxAdapter(child: VideoList()),
          SliverAppBar(
            leading: Container(),
            backgroundColor: CustomTheme.background,
            flexibleSpace:
                Container(margin: EdgeInsets.only(top: 5), child: VideoList()),
            collapsedHeight: MediaQuery.of(context).size.height * 0.4,
            expandedHeight: MediaQuery.of(context).size.height * 0.8,
          ),
          SliverToBoxAdapter(child: buildButtons(context)),
          exercise.repetitions.length > 0
              ? SliverList(
                  delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                    return makeRepetitionTile(
                        context, exercise.repetitions[index], index);
                  }, childCount: exercise.repetitions.length),
                )
              : SliverFillRemaining(
                  child: Container(
                    color: CustomTheme.background,
                  ),
                ),
        ],
      ),
    );
  }

  Duration? startRepTime;
  Duration? endRepTime;

  Widget buildButtons(BuildContext context) {
    var exercise = context.read<CreateExerciseViewModel>();
    return Container(
      color: CustomTheme.background,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            child: Visibility(
              visible: exercise.isAllVideosWithDescriptionAndVideo,
              child: ElevatedButton.icon(
                //   style: CustomTheme.elevatedButtonStyle,
                icon: Icon(Icons.arrow_drop_down_circle_rounded),
                label: Text("Mark beginning"),
                style: CustomTheme.primaryButtonStyle,
                onPressed: exercise.isAllVideosWithDescriptionAndVideo
                    ? () {
                        print("onPresset:");
                        print(exercise.currentController);
                        setState(() {
                          startRepTime = exercise.currentController
                              .videoPlayerController?.value.position;
                        });
                      }
                    : null,
              ),
            ),
          ),
          Expanded(
            child: Visibility(
              visible: exercise.isAllVideosWithDescriptionAndVideo,
              child: ElevatedButton.icon(
                style: CustomTheme.primaryButtonStyle,
                icon: Icon(Icons.arrow_drop_up_rounded),
                label: Text("Mark end"),
                onPressed: (startRepTime != null)
                    ? () {
                        endRepTime = exercise.currentController
                            .videoPlayerController?.value.position;

                        exercise.addRepetition(startRepTime, endRepTime);

                        startRepTime = null;
                        endRepTime = null;
                        setState(() {});
                      }
                    : null,
              ),
            ),
          ),
        ],
      ),
    );
  }

  final TextStyle timeLabelStyle = TextStyle(
      color: Colors.black, fontWeight: FontWeight.normal, fontSize: 12);

  final TextStyle timeStampStyle =
      TextStyle(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 13);

  Container makeRepetitionTile(
          BuildContext context, Repetition rep, int index) =>
      Container(
        margin: EdgeInsets.only(left: 10, right: 10),
        child: Card(
          elevation: 15,
          color: Colors.white.withAlpha(200),
          // color: CustomTheme.secondaryColor,
          child: ExpansionTile(
            leading: Container(
              padding: EdgeInsets.only(right: 12.0),
              decoration: new BoxDecoration(
                  border: new Border(
                      right: new BorderSide(width: 1.0, color: Colors.black))),
              child: Text(index.toString(),
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.w900)),
            ),
            children: [Text("VBT")],
            title: Column(
              children: [
                Row(children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      // color: Colors.red,
                      child: Text("Starts at ", style: timeLabelStyle),
                    ),
                  ),
                  Expanded(
                      flex: 2,
                      child: Container(
                          // color: Colors.green,
                          child: Text("${rep.startTime?.format()}",
                              style: timeStampStyle)))
                ]),
                Row(children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      // color: Colors.red,
                      child: Text("Ends at ", style: timeLabelStyle),
                    ),
                  ),
                  Expanded(
                      flex: 2,
                      child: Container(
                          // color: Colors.green,
                          child: Text("${rep.endTime?.format()}",
                              style: timeStampStyle)))
                ])
              ],
            ),
            //   subtitle: Row(
            //     children: <Widget>[
            //   Icon(Icons.linear_scale, color: Colors.yellowAccent),
            //   Text(" Intermediate", style: TextStyle(color: Colors.white))
            //     ],
            //   ),
            // );
            trailing: IconButton(
              icon: Icon(Icons.delete_outline),
              color: Colors.redAccent,
              onPressed: () {
                var exercise = context.read<CreateExerciseViewModel>();
                exercise.removeRepetition(rep);
              },
            ),
          ),
        ),
      );
}
