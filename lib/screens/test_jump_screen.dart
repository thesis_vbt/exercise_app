import 'package:app_video/theme/custom_theme.dart';
import 'package:app_video/widgets/navigation_drawer_wiget.dart';
import 'package:app_video/widgets/results_button_widget.dart';
import 'package:app_video/widgets/video_player_widget.dart';
import 'package:better_player/better_player.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:tflite/tflite.dart';

class TestJumpScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _TestJumpScreenState();
}

class _TestJumpScreenState extends State<TestJumpScreen> {
  @override
  void initState() {
    super.initState();
  }

  Duration? _takeOffTime;
  Duration? _landingTime;

  ChewieController? _playerController;
  String? _filePath;

  bool _showAnalyzeButton = false;
  bool _showResultsButton = false;
  bool _showProgressIndicator = false;

  Widget _buildVideoPlayer() {
    var videoPlayer = VideoPlayer(
      index: 0,
      aspectRatio: 9 / 16,
      hidePerspectiveDescriptionForm: true,
      onPerspectiveChanged: (text) {
        {
          print("onPerspectiveChanged: $text");
        }
      },
      onSourceChange: (filePath) {
        _filePath = filePath;
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text("Video source updated")));
        setState(() {});
      },
      onControllerChange: (controller) {
        _playerController = controller;
      },
    );

    return videoPlayer;
  }

  Widget _buildButtons() => Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: ElevatedButton.icon(
                onPressed: _playerController?.videoPlayerController != null
                    ? () {
                        setState(() {
                          _takeOffTime = _playerController
                              ?.videoPlayerController?.value.position;
                        });
                      }
                    : null,
                icon: Icon(Icons.arrow_upward),
                label: Text("Take-off"),
              ),
            ),
            Expanded(
              child: ElevatedButton.icon(
                onPressed: _playerController?.videoPlayerController != null &&
                        _takeOffTime != null
                    ? () {
                        setState(() {
                          _landingTime = _playerController
                              ?.videoPlayerController?.value.position;
                          _showAnalyzeButton = true;
                        });
                      }
                    : null,
                icon: Icon(Icons.arrow_downward),
                label: Text("Landing"),
              ),
            )
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomTheme.background,
      appBar: PreferredSize(
        preferredSize: const Size(double.infinity, kToolbarHeight),
        child: Builder(
          builder: (context) => AppBar(
              backgroundColor: CustomTheme.background,
              title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Jump test",
                      style: CustomTheme.headline,
                    ),
                    Stack(children: [
                      Visibility(
                          visible: _showAnalyzeButton,
                          child: ElevatedButton.icon(
                              onPressed: () {
                                setState(() {
                                  _showAnalyzeButton = false;
                                  _showProgressIndicator = true;
                                  _showResultsButton = false;
                                });
                              },
                              icon: Icon(Icons.analytics),
                              label: Text("Analyze!"))),
                      Visibility(
                          visible: _showProgressIndicator,
                          child: LinearProgressIndicator()),
                      Visibility(
                          visible: _showResultsButton,
                          child: ElevatedButton.icon(
                              onPressed: () {},
                              icon: Icon(Icons.ac_unit),
                              label: Text("")))
                    ])
                  ]),
              actions: [],
              leading: Container(
                  child: IconButton(
                      onPressed: () {
                        Scaffold.of(context).openDrawer();
                      },
                      icon:
                          Icon(Icons.menu, color: CustomTheme.primaryColor)))),
        ),
      ),
      drawer: NavigationDrawer(),
      body: Container(
        margin: EdgeInsets.only(top: 1),
        color: CustomTheme.background,
        child: Column(
          children: [
            _buildVideoPlayer(),
            _buildButtons(),
          ],
        ),
      ),
    );
  }
}
