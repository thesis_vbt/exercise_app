import 'package:app_video/models/repetition.dart';
import 'package:app_video/services/webapi/web_api.dart';
import 'package:app_video/theme/custom_theme.dart';
import 'package:app_video/widgets/repetitions_list_widget.dart';
import 'package:app_video/widgets/video_list.dart';
import 'package:better_player/better_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:app_video/models/exercise.dart';
import 'package:get_it/get_it.dart';
import 'package:chewie/chewie.dart';
import 'package:video_player/video_player.dart';

import 'package:prompt_dialog/prompt_dialog.dart';

import '../constants.dart';

class ExerciseDetailsScreen extends StatefulWidget {
  final Exercise exercise;

  const ExerciseDetailsScreen({Key? key, required this.exercise})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => _ExerciseDetailsScreenState();
}

class _ExerciseDetailsScreenState extends State<ExerciseDetailsScreen> {
  bool _showAnalyzeButton = true;
  bool _showResultsButton = false;
  bool _showProgressIndicator = false;
  int _personHeight = 0;
  List<List<double>> _repetitionResults = [];

  late final VideoPlayerController videoPlayerController;
  late final ChewieController chewieController;

  String _processButtonText = "Pre-process!";
  @override
  void initState() {
    super.initState();
    videoPlayerController =
        VideoPlayerController.network(widget.exercise.videos[0].videoFile);
    chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      autoPlay: false,
      looping: false,
    );
    if (widget.exercise.videos[0].poses.length == 0) {
      _showAnalyzeButton = true;
    } else {
      _showAnalyzeButton = false;
      _showResultsButton = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: CustomTheme.background,
        child: Column(mainAxisSize: MainAxisSize.max, children: [
          Container(
            margin: EdgeInsets.only(top: 40, left: 20),
            height: 50,
            child: Row(
              children: [
                Flexible(
                    child: Text(
                  widget.exercise.name,
                  style: CustomTheme.headline,
                )),
                Expanded(
                  child: Container(
                    child: Stack(children: [
                      Visibility(
                        visible: _showAnalyzeButton,
                        child: ElevatedButton.icon(
                            onPressed: () async {
                              var api = GetIt.I.get<WebApi>();
                              setState(() {
                                _showAnalyzeButton = false;
                                _showProgressIndicator = true;
                                _showResultsButton = false;
                              });

                              var video = api
                                  .callOpenpose(widget.exercise.videos[0].id!)
                                  .then((value) {
                                if (value != null) {
                                  print(value);
                                  widget.exercise.videos[0].poses = value.poses;
                                  setState(() {
                                    _showResultsButton = true;
                                    _showAnalyzeButton = false;
                                    _showProgressIndicator = false;
                                  });
                                }
                              }).onError((error, stackTrace) {
                                //   Revert to Openpose button if it failed
                                setState(() {
                                  _showAnalyzeButton = true;
                                  _showProgressIndicator = false;
                                  _showResultsButton = false;
                                });

                                ScaffoldMessenger.of(context)
                                    .showSnackBar(SnackBar(
                                  content: Text(error.toString()),
                                ));
                              });
                            },
                            icon: Icon(Icons.analytics),
                            label: Text("Estimate pose")),
                      ),
                      Visibility(
                          visible: _showProgressIndicator,
                          child: LinearProgressIndicator(
                            backgroundColor: Colors.transparent,
                            valueColor: AlwaysStoppedAnimation<Color>(
                              CustomTheme.primaryColor,
                            ),
                          )),
                      Visibility(
                          visible: _showResultsButton,
                          child: Row(
                            children: [
                              ElevatedButton.icon(
                                  onPressed: () async {
                                    if (_personHeight < 1) {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(SnackBar(
                                              content: Text(
                                                  "Height must be greater than 0!")));
                                      return;
                                    }
                                    var api = GetIt.I.get<WebApi>();
                                    // try {

                                    try {
                                      var repetitionResults =
                                          await api.calculateJumpMetrics(
                                              widget.exercise.id!,
                                              _personHeight);

                                      setState(() {
                                        _repetitionResults = repetitionResults;
                                        print(_repetitionResults);
                                      });
                                    } catch (exception) {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(SnackBar(
                                        content: Text(exception.toString()),
                                      ));
                                    }

                                    // } catch (e) {
                                    //   ScaffoldMessenger.of(context)
                                    //       .showSnackBar(SnackBar(
                                    //           content: Text(e.toString())));
                                    // }
                                  },
                                  icon: Icon(Icons.ac_unit),
                                  label: Text("Analyze!")),
                              Expanded(
                                child: TextField(
                                    onChanged: (value) =>
                                        _personHeight = int.parse(value),
                                    decoration: InputDecoration(
                                        labelStyle: CustomTheme.body1,
                                        labelText: "Height in cm"),
                                    keyboardType: TextInputType.number,
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly
                                    ]),
                              )
                            ],
                          ))
                    ]),
                  ),
                )
              ],
            ),
          ),
          Expanded(
              child: Chewie(
            controller: chewieController,
          )),
          Expanded(
            child: ListView.builder(
                itemCount: widget.exercise.repetitions.length,
                itemBuilder: (context, index) {
                  if (_repetitionResults.length > 0) {
                    return makeRepTileWithMetrics(
                        context,
                        widget.exercise.repetitions[index],
                        _repetitionResults[index][0],
                        _repetitionResults[index][1],
                        index);
                  }
                  return makeRepetitionTile(
                      context, widget.exercise.repetitions[index], index);
                }),
          ),
        ]),
        //   bottomNavigationBar: CustomAppBar()
      ),
    );
  }

  Container makeRepTileWithMetrics(BuildContext context, Repetition rep,
          double height, double flightTime, int index) =>
      Container(
          child: Card(
              elevation: 15,
              color: Colors.white.withAlpha(200),
              child: ExpansionTile(
                leading: Container(
                  padding: EdgeInsets.only(right: 12.0),
                  decoration: new BoxDecoration(
                      border: new Border(
                          right:
                              new BorderSide(width: 1.0, color: Colors.black))),
                  child: Text(index.toString(),
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.w900)),
                ),
                title: Container(
                    child: Row(
                  children: [
                    Text(
                        "You jumped ${height.toStringAsFixed(2)} cm for ${flightTime.toStringAsFixed(2)} sec")
                  ],
                )),
              )));

  Container makeRepetitionTile(
          BuildContext context, Repetition rep, int index) =>
      Container(
        margin: EdgeInsets.only(left: 10, right: 10),
        child: Card(
          elevation: 15,
          color: Colors.white.withAlpha(200),
          child: ExpansionTile(
            leading: Container(
              padding: EdgeInsets.only(right: 12.0),
              decoration: new BoxDecoration(
                  border: new Border(
                      right: new BorderSide(width: 1.0, color: Colors.black))),
              child: Text(index.toString(),
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.w900)),
            ),
            children: [Text("VBT")],
            title: InkWell(
              onTap: () {
                videoPlayerController.seekTo(rep.startTime!);
                videoPlayerController.addListener(() {
                  if (videoPlayerController.value.position >= rep.endTime!) {
                    print("Listener called");
                  } else if (videoPlayerController.value.position ==
                      rep.endTime) {
                    videoPlayerController.seekTo(rep.startTime!);
                  }
                });
              },
              child: Column(
                children: [
                  Row(children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        // color: Colors.red,
                        child: Text("Starts at "),
                      ),
                    ),
                    Expanded(
                        flex: 2,
                        child: Container(
                            // color: Colors.green,
                            child: Text(
                          "${rep.startTime?.format()}",
                        )))
                  ]),
                  Row(children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        // color: Colors.red,
                        child: Text(
                          "Ends at ",
                        ),
                      ),
                    ),
                    Expanded(
                        flex: 2,
                        child: Container(
                            // color: Colors.green,
                            child: Text(
                          "${rep.endTime?.format()}",
                        )))
                  ])
                ],
              ),
            ),
          ),
        ),
      );
}
