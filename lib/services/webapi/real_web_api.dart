import 'dart:convert';
import 'dart:io';
import 'package:app_video/main.dart';
import 'package:app_video/models/exercise.dart';
import 'package:app_video/models/pose_server_config.dart';
import 'package:app_video/models/video.dart';
import 'package:app_video/services/webapi/web_api.dart';
import 'package:http/http.dart' as http;
import 'package:app_video/file_extensions.dart';

import '../../constants.dart';

class RealWebApi extends WebApi {
  String nextPageUrl = "";
  @override
  void resetPage() {
    nextPageUrl = "";
  }

  Future<List<PoseServerConfig>> fetchServerConfigs() async {
    var response =
        await http.get(Uri.http(API_BASE_URL, "pose_server_config/"));

    if (response.statusCode == 200) {
      print(response.body);
      return List.empty();
    } else if (response.statusCode == 304) {
      throw ("This video was already processed!");
    } else {
      throw ("Error processing file!");
    }
  }

  Future<List<List<double>>> calculateJumpMetrics(
      int exerciseId, int height) async {
    var response = await http.get(
        Uri.http(API_BASE_URL, "exercises/$exerciseId/calculate_jump_metrics/"),
        headers: <String, String>{
          'Person-Height': height.toString(),
        });
    print(response.statusCode);
    List<dynamic> tmp = jsonDecode(response.body);
    List<List<double>> rtn = [];

    for (List<dynamic> t in tmp) {
      rtn.add([t[0], t[1]]);
    }
    switch (response.statusCode) {
      case 200:
        return rtn;
      case 500:
        throw ("There was an error processing the request. Please try again later.");
      default:
        throw ("Unexpected error handling request. Server returned: ${response.statusCode}");
    }
  }

  Future<Video?> callOpenpose(int videoId) async {
    var response = await http
        .get(Uri.http(API_BASE_URL, "videos/$videoId/process_openpose/"));

    if (response.statusCode == 200) {
      return Video.fromJson(jsonDecode(response.body));
    } else if (response.statusCode == 304) {
      throw ("This video was already processed!");
    } else {
      throw ("Error processing file with OpenPose!");
    }
  }

  @override
  Future<List<Exercise>> fetchNextPage() async {
    Uri target;
    if (nextPageUrl == null) {
      return List<Exercise>.empty();
    } else if (nextPageUrl == "") {
      target = Uri.http(API_BASE_URL, "exercises/");
    } else {
      target = Uri.parse(nextPageUrl);
    }
    var response = await http.get(target, headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    });

    Map<String, dynamic> responseJson = jsonDecode(response.body);
    print("next: ${responseJson["next"]}");
    nextPageUrl = responseJson["next"] ?? "";
    responseJson.remove("count");
    responseJson.remove("next");
    responseJson.remove("previous");
    List<dynamic> results = responseJson["results"];
    List<Exercise> exercises = [];
    results.forEach((element) {
      exercises.add(Exercise.fromJson(element));
    });

    print(exercises);
    return exercises;
  }

  @override
  Future<void> saveExercise(Exercise exercise) async {
    // var json = jsonEncode(exercise);
    var exerciseDict = exercise.toJson();
    var videos = exerciseDict["videoFiles"];
    String videosJson =
        jsonEncode(exercise.videos.map((e) => e.toJson()).toList());
    exerciseDict.remove("id");
    exerciseDict.remove("videoFiles");
    print(videos);
    print(exerciseDict);
    var response = await http.post(
      Uri.http(API_BASE_URL, 'exercises/'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(exerciseDict),
    );

    var responseJson = jsonDecode(response.body);
    print(responseJson);

    if (response.statusCode == 201) {
      int id = responseJson["id"];

      for (Video video in exercise.videos) {
        // var postUri = Uri.parse(
        // '/exercises/${responseJson["id"]}/video/');
        var postUri = Uri.http(API_BASE_URL, "exercises/$id/video/");
        var request = new http.MultipartRequest("PUT", postUri);
        request.fields['perspective_description'] =
            video.perspectiveDescription;
        File videoFile = File(video.videoFile);
        request.files.add(http.MultipartFile("video_file",
            videoFile.readAsBytes().asStream(), videoFile.lengthSync(),
            filename: videoFile.name));

        await request.send().then(
          (response) async {
            print(response.statusCode);
            String resp = await response.stream.bytesToString();
            print(resp);

            if (response.statusCode == 200) {
              return print("Uploaded!");
            } else {
              throw ("Failed to upload video(s)");
            }
          },
        );
      }
    } else {
      throw ("Error saving exercise.${response.body}");
    }
  }
}
