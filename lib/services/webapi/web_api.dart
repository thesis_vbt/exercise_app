import 'package:app_video/models/exercise.dart';
import 'package:app_video/models/exercise.dart';
import 'package:app_video/models/video.dart';

abstract class WebApi {
  bool hasMorePages = true;
  void resetPage();
  Future<List<List<double>>> calculateJumpMetrics(int exerciseId, int height);
  Future<List<Exercise>> fetchNextPage();
  Future<void> saveExercise(Exercise exercise);
  Future<Video?> callOpenpose(int videoId);
}
