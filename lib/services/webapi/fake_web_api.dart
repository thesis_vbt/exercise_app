import 'dart:convert';

import 'package:app_video/models/exercise.dart';
import 'package:app_video/models/video.dart';
import 'package:app_video/services/webapi/web_api.dart';

class FakeWebApi implements WebApi {
  @override
  Future<List<Exercise>> fetchNextPage() async {
    String jsonExercise = '''{
            "id": 1,
            "created_at": "2021-05-06T15:24:34.524239Z",
            "updated_at": "2021-05-06T15:24:34.524435Z",
            "repetitions": [{
                    "id": 5,
                    "exercise": 7,
                    "created_at": "2021-05-06T16:29:06.895500Z",
                    "updated_at": "2021-05-06T16:29:06.895613Z",
                    "start_timestamp": "00:00:00.144",
                    "end_timestamp": "00:00:00.58"
                }],
            "name": "hegdg",
            "videos": [],
            "thumbnail": null
        } 
    ''';

    Map<String, dynamic> m = jsonDecode(jsonExercise);
    Exercise exercise = Exercise.fromJson(m);
    print("From API, JSON -> Object:");
    print(exercise);
    return [exercise];
  }

  int currentPage = 0;
  @override
  bool hasMorePages = true;

  @override
  Future<void> saveExercise(Exercise exercise) {
    // TODO: implement saveExercise
    throw UnimplementedError();
  }

  @override
  void resetPage() {
    // TODO: implement resetPage
  }

  @override
  Future<Video?> callOpenpose(int videoId) {
    // TODO: implement callOpenpose
    throw UnimplementedError();
  }

  @override
  Future<List<List<double>>> calculateJumpMetrics(int exerciseId, int height) {
    // TODO: implement calculateJumpMetrics
    throw UnimplementedError();
  }
}
