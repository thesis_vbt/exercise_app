import 'package:app_video/services/webapi/fake_web_api.dart';
import 'package:app_video/services/webapi/real_web_api.dart';
import 'package:app_video/services/webapi/web_api.dart';
import 'package:get_it/get_it.dart';

GetIt serviceLocator = GetIt.instance;

void setupServiceLocator() {
  serviceLocator.registerLazySingleton<WebApi>(() => RealWebApi());
}
