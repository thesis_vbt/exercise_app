import 'package:flutter/foundation.dart';

class TestJumpViewModel extends ChangeNotifier {
  Duration? _takeOffTime;

  Duration? get takeOffTime => _takeOffTime;

  set takeOffTime(Duration? takeOffTime) {
    _takeOffTime = takeOffTime;
    notifyListeners();
  }

  Duration? _landingTime;

  Duration? get landingTime => _landingTime;

  set landingTime(Duration? landingTime) {
    _landingTime = landingTime;
    notifyListeners();
  }
}
