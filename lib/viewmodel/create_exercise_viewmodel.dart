import 'dart:convert';
import 'dart:io';
import 'package:app_video/models/exercise.dart';
import 'package:app_video/models/repetition.dart';
import 'package:app_video/models/video.dart';
import 'package:app_video/services/webapi/web_api.dart';
import 'package:better_player/better_player.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';

class CreateExerciseViewModel extends ChangeNotifier {
  Exercise _exercise = Exercise("");

//   BetterPlayerController _currentVideoController;
  int _currentControllerIndex = 0;
//   Map<int, BetterPlayerController> _videoControllers = {};
  List<ChewieController> _videoControllers = [];

  String get name => _exercise.name;
  set name(String name) => _exercise.name = name;
  List<Repetition> get repetitions => _exercise.repetitions;

  bool get hasVideos => _exercise.videos.length > 0;
  bool get isAllVideosWithDescriptionAndVideo {
    if (_exercise.videos.length == 0) return false;
    for (int i = 0; i < _exercise.videos.length; i++) {
      Video value = _exercise.videos[i];
      if (value.videoFile.isEmpty || value.perspectiveDescription.isEmpty)
        return false;
    }

    return true;
  }

  ChewieController get currentController {
    // ! TODO: change here to add multi video support
    return _videoControllers[0];
  }

  void addRepetition(Duration? start, Duration? end) {
    int i = 0;

    for (i = 0; i < _exercise.repetitions.length; i++) {
      if (_exercise.repetitions[i].startTime! > start!) {
        break;
      }
    }

    _exercise.repetitions.insert(i, Repetition(start, end));

    notifyListeners();
  }

  void removeRepetition(Repetition rep) {
    _exercise.repetitions.removeWhere((element) => element == rep);
    notifyListeners();
  }

  /// Called when a new Video Player widget is created
  ///
  /// Should be called from outside of the VideoPlayerWidget, since we cannot explicitly control is lifetime
  void registerVideoPlayer(int index, ChewieController controller) {
    // index = 0, length = 0 => Criar novo
    // index = 1, length = 1 => Criar novo
    if (_exercise.videos.length == index) {
      _exercise.videos.add(Video.empty());
      _videoControllers.add(controller);
    } else if (index < _exercise.videos.length) {
      // ! What do i need to do with the video file obj?
      _videoControllers[index] = controller;
    } else {
      throw ("Index is greater than current video files array's length. It shouldn't since at most, it should be equal.");
    }
  }

  // ! TODO: finish register and unregister video thing.
  // Problem is that I don't control the video player lifecycle, so sometimes it gets disposed

  /// Called when a Video Player widget is removed from the list
  ///
  /// It should NOT be unregistered on the dispose method! The lifetime of the VideoPlayer
  /// is controlled by the framework!
  void unregisterVideoPlayer(int index) {
    _exercise.videos.removeAt(index);
    _videoControllers.removeAt(index);
  }

//   void addVideo() {
//     _exercise.videos.add(Video.empty());
//   }

  void setVideoFile(File videoFile) {
    // ! TODO: change here to add multi video support
    if (_exercise.videos.length == 0) _exercise.videos.add(Video.empty());

    _exercise.videos[0].videoFile = videoFile.absolute.path;
    notifyListeners();
  }

  void setVideoFileDescription(int index, String perspectiveDescription) {
    // ! TODO: change here to add multi video support
    if (_exercise.videos.length == 0) _exercise.videos.add(Video.empty());

    _exercise.videos[index].perspectiveDescription = perspectiveDescription;
    print("setVideoFileDescription: ${_exercise.videos}");
    notifyListeners();
  }

  void removeVideo(int index) {
    _exercise.videos.removeAt(index);
  }

//   void addVideoController(BetterPlayerController controller) {
//     _videoControllers.add(controller);
//   }

  void setVideoController(int index, ChewieController controller) {
    // ! TODO: change this on multi-video...
    if (_videoControllers.length == 0) {
      _videoControllers.add(controller);
      //   notifyListeners();
    } else {
      _videoControllers[0] = controller;
    }
  }

  void removeVideoController(int index) {
    _videoControllers.removeAt(index);
  }

  void setCurrentVideoController(int index) {
    _currentControllerIndex = index;
  }

  Future<void> save() async {
    var api = GetIt.I.get<WebApi>();
    // print(_exercise.videoFiles);
    await api.saveExercise(_exercise);
    //   print(exercise.videos);
    //   print(exercise.name);
    //   var dict = {"name": exercise.name};
    //   var response = await http.post(
    //     Uri.http('192.168.1.78:8000', 'exercises/'),
    //     headers: <String, String>{
    //       'Content-Type': 'application/json; charset=UTF-8',
    //     },
    //     body: jsonEncode(dict),
    //   );

    //   print(response.body);

    //   // Add your onPressed code here!
  }
}
